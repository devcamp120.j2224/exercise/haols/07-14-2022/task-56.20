import com.devcamp.S10.Task5610.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle Rec1 = new Rectangle();
        Rectangle Rec2 = new Rectangle(3.0f, 2.0f);
        System.out.println(Rec1.toString());
        System.out.println(Rec2.toString());
        float area1 = Rec1.getArea();
        float area2 = Rec2.getArea();
        float Peri1 = Rec1.getPerimeter();
        float Peri2 = Rec2.getPerimeter();
        System.out.println("Rectangle 1 :Area= " + area1 + " Perimeter= " + Peri1);
        System.out.println("Rectangle 2 :Area= " + area2 + " Perimeter= " + Peri2);
    }
}
